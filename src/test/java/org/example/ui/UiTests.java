package org.example.ui;

import com.codeborne.selenide.*;
import com.example.generator.UserFactory;
import com.example.generator.UserType;
import com.example.models.AppUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class UiTests {

    @BeforeAll
    public static void setUpTests(){
        Configuration.timeout = 11000;
        Configuration.browser = "chrome";
        Configuration.baseUrl = "http://localhost:8080";
    }

    @AfterEach
    public void cleanupBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }

    /*@Test
    public void nonRefactored(){
        open("/");
        var loginField =$("#loginInput");
        loginField.shouldBe(visible).parent().click();
        loginField.shouldBe(enabled).setValue("test");
        var passwordField = $("#passwordInput");
        passwordField.parent().click();
        passwordField.shouldBe(enabled).setValue("test");
        var confirmButton = $("div[class*='card-footer'] button:nth-child(2)");
        confirmButton.shouldBe(enabled).shouldHave(text("Hover me faster!\n" + "            "));
        confirmButton.hover();
        confirmButton.shouldHave(text("Wait for some time..."));
        $("img[src='sign.png']").parent().shouldBe(visible).click();
        Alert loginAlert = switchTo().alert();
        Assertions.assertEquals("Are you sure you want to login?", loginAlert.getText());
        loginAlert.accept();
        $("#loader").shouldBe(visible);
        Alert confirmAlert = switchTo().alert();
        Assertions.assertEquals("Really sure?", confirmAlert.getText());
        loginAlert.accept();
        $("#loader").should(disappear);
        $("#avatar").shouldBe(visible);
    }*/

    @Test
    public void shouldLogin(){
        new LoginPage().open()
                .loginAs(UserFactory.getUser(UserType.ADMIN));
        at(MainPage.class).accountButton.shouldBe(visible);
    }

    private <T> T at(Class<T> tClass) {
        return page(tClass);
    }

    @Test
    public void testWebdriverInstance(){
        new LoginPage().open();
        var driver = WebDriverRunner.getWebDriver();
        if (driver instanceof ChromeDriver chromeDriver){
            System.out.println(chromeDriver.getSessionId());
        }
    }

}

class LoginPage {

    public SelenideElement loginField = $("#loginInput"),
                        passwordField = $("#passwordInput"),
                        loader = $("#loader"),
                        submitButton = $("div[class*='card-footer'] button:nth-child(2)"),
                        signInButton = $("img[src='sign.png']").$x("parent::node()"); //.$x("..");


    public LoginPage open(){
        return Selenide.open("/", this.getClass());
    }

    public MainPage loginAs(AppUser user){
        loginField.shouldBe(visible).parent().click();
        loginField.shouldBe(enabled).setValue(user.username());
        passwordField.parent().click();
        passwordField.shouldBe(enabled).setValue(user.password());
        submitButton.shouldBe(enabled).shouldHave(text("Hover me faster!\n" + "            "));
        submitButton.hover();
        submitButton.shouldHave(text("Wait for some time..."));
        signInButton.shouldBe(visible).click();
        Alert loginAlert = switchTo().alert();
        Assertions.assertEquals("Are you sure you want to login?", loginAlert.getText());
        loginAlert.accept();
        loader.shouldBe(visible);
        Alert confirmAlert = switchTo().alert();
        Assertions.assertEquals("Really sure?", confirmAlert.getText());
        loginAlert.accept();
        loader.should(disappear);
        return new MainPage();
    }


}

class MainPage {

    public SelenideElement accountButton = $("#avatar");

}
