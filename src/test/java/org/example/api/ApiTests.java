package org.example.api;

import com.example.models.Post;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.fest.assertions.Assertions.*;


public class ApiTests {


    public static RequestSpecification requestSpec;


    @BeforeAll
    public static void setUpTests(){
        var baseUrl = "https://jsonplaceholder.typicode.com";
        requestSpec = new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setBasePath("posts")
                .setRelaxedHTTPSValidation()
                .addFilters(List.of(new RequestLoggingFilter(), new ResponseLoggingFilter()))
                .setContentType(ContentType.JSON).build();
    }

    @Test
    public void apiTestUsingJsonString(){
        var myPost = """
                        {
                            "userId": %s,
                            "title": "test post",
                            "body": "Hello World!"
                        }       
                """.formatted(1);
        given().spec(requestSpec)
                .body(myPost)
                .when().post("/")
                .then()
                .assertThat().statusCode(201)
                .body("id", equalTo(101));

    }


    @Test
    public void apiTestUsingRecord (){
        var myPost = new Post(null, 1, "test post", "Hello World");
        var post = given().spec(requestSpec)
                .body(myPost, ObjectMapperType.GSON)
                .when().post("/")
                .then()
                .assertThat().statusCode(201)
                //.body("id", equalTo(101));
                .and().extract().body().as(Post.class, ObjectMapperType.GSON);
        assertAll(() -> {
            assertEquals(101, post.id());
            assertEquals(myPost.title(), post.title());
            assertEquals(myPost.userId(), post.userId());
            assertEquals(myPost.body(), post.body());
        });
    }

    @Test
    public void festAssertPostsListTest(){
        var myPost = new Post(null, 1, "test post", "Hello World");
        assertThat(given().spec(requestSpec).when()
                .get().thenReturn().as(Post[].class, ObjectMapperType.GSON))
                .hasSize(100)
                .hasAllElementsOfType(Post.class)
                .excludes(myPost);
    }

    @Test
    public void festAssertPostTest(){
        var post = given().spec(requestSpec).when()
                .get("/1").thenReturn().as(Post.class, ObjectMapperType.GSON);
        assertThat(post).isInstanceOf(Post.class);
        assertThat(post.title()).startsWith("sunt aut facere");
        assertThat(post.body()).contains("quia et suscipit");
        assertThat(post.id()).isEqualTo(1);
    }


    @Test
    public void testSql(){
        /*language=SQL*/
        var sql = """
                SELECT * 
                FROM users 
                """.formatted(1);
        System.out.println(sql);
    }
}


class PostResponse {

}