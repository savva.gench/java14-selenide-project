package com.example.generator;

import com.example.models.AppUser;

public class UserFactory {

    public static AppUser getUser(UserType userType){
        return switch (userType){
            case ADMIN -> new AppUser("test", "test");
            case GUEST -> new AppUser("guest", "guest");
            default -> throw new IllegalStateException("Unexpected Invalid user: " + userType);
        };
    }

}
